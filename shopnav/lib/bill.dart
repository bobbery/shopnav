import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:shopnav/main.dart';
import 'map.dart';
import 'data.dart';

class BillPage extends StatefulWidget {
  BillPage(this.shopId);

  final String title = "ShopNav";
  final int shopId;

  @override
  _BillPageState createState() => _BillPageState();
}

class _BillPageState extends State<BillPage> {
  bool productsLoaded = false;
  SearchBar searchBar;
  String filter = "";

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => MyHomePage())),
        ),
        title: Text(widget.title + " - " + finalSum.toStringAsFixed(2) + " €"),
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    setState(() {
      filter = value;
    });
  }

  _BillPageState() {
    searchBar = SearchBar(
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted,
        onCleared: () {
          print("cleared");
        },
        onClosed: () {
          print("closed");
        });
  }

  @override
  void initState() {
    finalSum = 0.0;
    getProducts();
    super.initState();
  }

  void getProducts() {
    http
        .get(Uri.parse(
            baseUrl + 'getRes.php?type=products&shopId=${widget.shopId}'))
        .then((result) {
      print(result.statusCode);
      if (result.statusCode == 200) {
        print(jsonDecode(result.body));

        var tagObjsJson = jsonDecode(result.body) as List;

        setState(() {
          Data.items =
              tagObjsJson.map((tagJson) => Item.fromJson(tagJson)).toList();
          finalSum = 0.0;
          productsLoaded = true;

          for (Item itm in Data.items) {
            itm.count = 0;
            if (itm.nameEN == "START" || itm.nameEN == "END") {
              itm.count = 1;
            }
          }
        });
      }
    });
  }

  bool filterMatch(Item item) {
    var filters = filter.split(" ");
    bool contained = true;
    for (String flt in filters) {
      if (item.name.toUpperCase().contains(flt.toUpperCase()) ||
          item.nameEN.toUpperCase().contains(flt.toUpperCase())) {
      } else {
        contained = false;
      }
    }

    return contained;
  }

  List<Widget> getItemWidgets() {
    List<Widget> items = [];

    finalSum = 0.0;
    for (var item in Data.items) {
      if (item.nameEN == "START" || item.nameEN == "END") {
        item.count = 1;
        continue;
      }
      finalSum += item.count * item.costs;
      if (filterMatch(item)) {
        items.add(ListTile(
          leading: Icon(Icons.add_shopping_cart),
          title: Text(item.name),
          subtitle: Text(item.costs.toStringAsFixed(2)),
          trailing: Container(
            width: 100,
            height: 50,
            child: Row(
              children: [
                InkWell(
                    onTap: () {
                      setState(() {
                        if (item.count > 0) {
                          item.count--;
                        }

                        finalSum = 0.0;
                        for (var item in Data.items) {
                          finalSum += item.count * item.costs;
                        }
                      });
                    },
                    child: Icon(Icons.remove, color: Colors.red, size: 35)),
                Text(item.count.toString()),
                InkWell(
                    onTap: () {
                      setState(() {
                        item.count += 1;
                        finalSum = 0.0;
                        for (var item in Data.items) {
                          finalSum += item.count * item.costs;
                        }
                      });
                    },
                    child: Icon(Icons.add, color: Colors.green, size: 35)),
              ],
            ),
          ),
        ));
      }
    }

    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: searchBar.build(context),
      body: productsLoaded
          ? ListView(children: getItemWidgets())
          : Text("Loading..."),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => MapPage(widget.shopId)));
        },
        label: const Text('Go shopping'),
        icon: const Icon(Icons.arrow_forward),
        backgroundColor: Colors.lightGreen,
      ),
    );
  }
}
