import 'dart:math';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shopnav/main.dart';
import 'data.dart';

class MapPage extends StatefulWidget {
  MapPage(this.shopId);

  final String title = "ShopNav";
  final int shopId;

  @override
  MapPageState createState() => MapPageState();
}

class MapPageState extends State<MapPage> with TickerProviderStateMixin {
  TransformationController _transformationController =
      TransformationController();

  double xPos = 0;
  double yPos = 0;
  int _index = 0;

  Image pic;

  BoxConstraints size = BoxConstraints();

  @override
  void initState() {
    loadImage();
    super.initState();
  }

  void loadImage() async {
    try {
      pic = Image.network(
        baseUrl + 'getRes.php?type=images&shopId=${widget.shopId}',
        fit: BoxFit.contain,
      );
    } catch (e) {
      pic = Image.asset(
        'img/grundriss.png',
        fit: BoxFit.contain,
      );
    }

    Timer(const Duration(milliseconds: 400), () {
      setState(() {
        goToPos();
      });
    });

    setState(() {});
  }

  void goToPos() async {
    double x = Data.items[_index].x;
    double y = Data.items[_index].y;

    x = x + 50 / 2 - size.maxWidth / 2;
    y = y + 50 / 2 - size.maxHeight / 2;

    var scale = 0.3;

    var end = Matrix4.identity()..scale(scale);

    var animationController1 =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);

    var animation1 =
        Matrix4Tween(begin: _transformationController.value, end: end)
            .animate(animationController1);

    setState(() {
      animationController1.addListener(() {
        _transformationController.value = (animation1.value);
      });
      animationController1.forward();
    });

    animationController1.addStatusListener((status) {
      end = Matrix4.identity()..translate(x, y);

      var animationController2 = AnimationController(
          duration: const Duration(seconds: 2), vsync: this);

      var animation2 = Matrix4Tween(
              begin: _transformationController.value,
              end: Matrix4.inverted(end))
          .animate(animationController2);

      setState(() {
        animationController2.addListener(() {
          _transformationController.value = (animation2.value);
        });
        animationController2.forward();
      });
    });
  }

  List<Widget> overlay() {
    List<Widget> widgets = [];
    widgets.add(pic);
    double x = Data.items[_index].x;
    double y = Data.items[_index].y;
    widgets.add(Positioned(
        left: x - 25,
        top: y - 25,
        child: Column(children: [
          Icon(
            Icons.shopping_cart,
            size: 50,
            color: Colors.redAccent,
          ),
        ])));
    for (Item i in Data.items) {
      widgets.add(Positioned(
          left: i.x - 5,
          top: i.y - 5,
          child: Icon(Icons.star, size: 10, color: Colors.black)));
    }

    return widgets;
  }

  bool hasMoreItems() {
    for (int i = _index + 1; i < Data.items.length; i++) {
      if (Data.items[i].count > 0) {
        return true;
      }
    }
    return false;
  }

  Widget map() {
    return InteractiveViewer(
        minScale: 0.1,
        maxScale: 10.0,
        constrained: false,
        transformationController: _transformationController,
        child: Stack(
          children: overlay(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Data.items[_index].count.toString() +
            "x - " +
            Data.items[_index].name),
      ),
      body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        size = constraints;
        return map();
      }),
      floatingActionButton: Row(
        children: [
          Container(width: 20, height: 10),
          FloatingActionButton.extended(
              label: Text('Previous Item'),
              icon: const Icon(Icons.arrow_back),
              backgroundColor: Colors.lightGreen,
              onPressed: () {
                setState(() {
                  if (_index > 0) {
                    _index--;
                  }
                  while (_index > 0 && Data.items[_index].count == 0) {
                    _index--;
                  }
                  goToPos();
                });
              }),
          Spacer(),
          FloatingActionButton.extended(
            onPressed: () {
              if (!hasMoreItems()) {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => MyHomePage()));
              } else {
                setState(() {
                  if (_index < Data.items.length) {
                    _index++;
                  }
                  while (_index < Data.items.length &&
                      Data.items[_index].count == 0) {
                    _index++;
                  }
                  goToPos();

                  if (Data.items[_index].nameEN == "END") {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                              title: Text('Final Sum: '),
                              content: Container(
                                height: 50,
                                width: 300,
                                child: Text(
                                    "You have to pay: ${finalSum.toStringAsFixed(2)} €"),
                              ),
                              actions: [
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text("OK"))
                              ]);
                        });
                  }
                });
              }
            },
            label: hasMoreItems() ? Text('Next item') : Text('Exit store'),
            icon: const Icon(Icons.arrow_forward),
            backgroundColor: Colors.lightGreen,
          ),
        ],
      ),
    );
  }
}
