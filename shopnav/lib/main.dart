import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:http/http.dart' as http;
import 'bill.dart';
import 'data.dart';

void main() {
  runApp(MyApp());
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: MyHomePage(),
      title: Text(
        'ShopNav',
        textScaleFactor: 4,
      ),
      image: Image(image: AssetImage('img/logo.png')),
      loadingText: Text("Loading ..."),
      photoSize: 150.0,
      loaderColor: Colors.black,
      backgroundColor: Colors.lightGreen,
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ShopNav',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: Splash(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  final String title = "ShopNav";

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<ShopItem> shopItems = [];
  String filter = "";
  SearchBar searchBar;

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        title: new Text('ShopNav'),
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    setState(() {
      filter = value;
    });
  }

  _MyHomePageState() {
    searchBar = SearchBar(
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted,
        onCleared: () {
          print("cleared");
        },
        onClosed: () {
          print("closed");
        });
  }

  @override
  void initState() {
    super.initState();
    loadShops();
  }

  void loadShops() {
    http.get(Uri.parse(baseUrl + 'getRes.php?type=shops')).then((result) {
      print(result.statusCode);
      if (result.statusCode == 200) {
        print(jsonDecode(result.body));

        var tagObjsJson = jsonDecode(result.body) as List;

        setState(() {
          shopItems =
              tagObjsJson.map((tagJson) => ShopItem.fromJson(tagJson)).toList();
        });
      }
    });
  }

  bool filterMatch(ShopItem item) {
    var filters = filter.split(" ");
    bool contained = true;
    for (String flt in filters) {
      if (item.name.toUpperCase().contains(flt.toUpperCase()) ||
          item.place.toUpperCase().contains(flt.toUpperCase()) ||
          item.street.toUpperCase().contains(flt.toUpperCase())) {
      } else {
        contained = false;
      }
    }

    return contained;
  }

  List<Widget> shopList() {
    List<Widget> result = [];

    for (ShopItem item in shopItems) {
      if (filterMatch(item)) {
        result.add(ListTile(
          leading: Icon(Icons.storefront),
          title: Text(item.name),
          subtitle: Text(item.place + ', ' + item.street),
          onTap: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => BillPage(item.shopId)));
          },
        ));
      }
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: searchBar.build(context),
      body: ListView(
        children: shopList(),
      ),
    );
  }
}
