import 'dart:math';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart';
import 'package:shopplan/main.dart';
import 'data.dart';

class MapPage extends StatefulWidget {
  MapPage(int shopId) : shopId = shopId;

  final String title = "ShopPlan";

  final int shopId;

  @override
  MapPageState createState() => MapPageState();
}

class MapPageState extends State<MapPage> with SingleTickerProviderStateMixin {
  TransformationController _transformationController =
      TransformationController();

  double xPos = 0;
  double yPos = 0;

  String name = "";
  String nameEN = "";
  double price = 0.0;
  Image pic;
  String imgFormat = ".png";
  bool imageChanged = false;

  final picker = ImagePicker();

  // File upload
  String base64Image;

  @override
  void initState() {
    super.initState();
    loadImage();
  }

  void loadImage() {
    try {
      pic = Image.network(
          baseUrl + 'getRes.php?type=images&shopId=${widget.shopId}',
          fit: BoxFit.contain);
    } catch (e) {
      pic = Image.asset('img/grundriss.png', fit: BoxFit.contain);
    }

    http
        .get(Uri.parse(
            baseUrl + 'getRes.php?type=products&shopId=${widget.shopId}'))
        .then((result) {
      print(result.statusCode);
      if (result.statusCode == 200) {
        print(jsonDecode(result.body));

        var tagObjsJson = jsonDecode(result.body) as List;

        setState(() {
          Data.items =
              tagObjsJson.map((tagJson) => Item.fromJson(tagJson)).toList();
        });
      }
    });
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        setState(() {
          pic = Image.network(pickedFile.path);
          pickedFile
              .readAsBytes()
              .then((value) => base64Image = base64Encode(value));
        });

        imageChanged = true;
      } else {
        print('No image selected.');
      }
    });
  }

  startUpload() {
    print('Uploading Settings...');
    uploadSettings("test.json");
  }

  uploadSettings(String fileName) {
    String data = jsonEncode(Data.items);

    http.post(Uri.parse(baseUrl + 'uploadProducts.php'), body: {
      "data": data,
      "shopId": '${widget.shopId}',
    }).then((result) {
      print(result.statusCode == 200 ? result.body : "ERROR");
      if (base64Image != null) {
        http.post(Uri.parse(baseUrl + 'uploadImage.php'), body: {
          "image": base64Image,
          "shopId": '${widget.shopId}',
        }).then((result) {
          print(result.statusCode == 200 ? result.body : "ERROR");
        }).catchError((error) {
          print(error);
        });
      }
    }).catchError((error) {
      print(error);
    });
  }

  List<Widget> _dialogButtons() {
    bool containsStart = false;
    bool containsEnd = false;

    for (Item i in Data.items) {
      if (i.nameEN == "START") {
        containsStart = true;
      }
      if (i.nameEN == "END") {
        containsEnd = true;
      }
    }

    List<Widget> buttons = [];
    if (!containsStart) {
      buttons.add(ElevatedButton(
        child: Text('START'),
        onPressed: () {
          setState(() {
            Item newItem = Item(-1, name, "START", 0, 1, xPos, yPos);
            Data.items.add(newItem);
            Navigator.pop(context);
          });
        },
      ));
    } else if (!containsEnd) {
      buttons.add(ElevatedButton(
        child: Text('END'),
        onPressed: () {
          setState(() {
            Item newItem = Item(-1, name, "END", 0, 1, xPos, yPos);
            Data.items.add(newItem);
            Navigator.pop(context);
          });
        },
      ));
    } else {
      buttons.add(ElevatedButton(
        child: Text('ADD'),
        onPressed: () {
          setState(() {
            Item newItem = Item(-1, name, nameEN, price, 1, xPos, yPos);
            Data.items.add(newItem);
            Navigator.pop(context);
          });
        },
      ));
    }
    buttons.add(ElevatedButton(
      child: Text('CANCEL'),
      onPressed: () {
        setState(() {
          Navigator.pop(context);
        });
      },
    ));
    return buttons;
  }

  Future<void> newItemDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('New Item'),
            content: Container(
              height: 180,
              width: 600,
              child: Column(children: [
                TextField(
                  onChanged: (value) {
                    name = value;
                  },
                  decoration: InputDecoration(hintText: "Name"),
                ),
                TextField(
                  onChanged: (value) {
                    nameEN = value;
                  },
                  decoration: InputDecoration(hintText: "NameEN"),
                ),
                TextField(
                  onChanged: (value) {
                    price = double.parse(value);
                  },
                  decoration: InputDecoration(labelText: "Price"),
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9\.]'))
                  ],
                ),
              ]),
            ),
            actions: _dialogButtons(),
          );
        });
  }

  void goToPos(BoxConstraints size) async {
    double x = max(0, xPos - min(size.maxWidth * 0.8, size.maxHeight));
    double y = max(0, yPos - min(size.maxWidth * 0.8, size.maxHeight));
    var end = Matrix4.identity()..translate(x, y);
    setState(() {
      _transformationController.value = Matrix4.inverted(end);
    });
  }

  Widget itemList(BoxConstraints size) {
    return Container(
        width: size.maxWidth * 0.2,
        color: Colors.lightGreen,
        child: ReorderableListView(
            buildDefaultDragHandles: false,
            padding: const EdgeInsets.symmetric(horizontal: 40),
            children: [
              for (int index = 0; index < Data.items.length; index++)
                InkWell(
                  key: Key('$index'),
                  onTap: () {
                    setState(() {
                      xPos = Data.items[index].x;
                      yPos = Data.items[index].y;
                      goToPos(size);
                    });
                  },
                  child: Card(
                      color: Data.items[index].nameEN == "START" ||
                              Data.items[index].nameEN == "END"
                          ? Colors.yellow
                          : Colors.greenAccent,
                      child: ListTile(
                          title: Text('${Data.items[index].name}'),
                          subtitle: Data.items[index].nameEN == "START" ||
                                  Data.items[index].nameEN == "END"
                              ? null
                              : Text(
                                  '${Data.items[index].costs.toStringAsFixed(2)}'),
                          trailing: ReorderableDragStartListener(
                              index: index, child: Icon(Icons.expand)),
                          leading: InkWell(
                            child: Icon(Icons.delete),
                            onTap: () {
                              setState(() {
                                Data.items.removeAt(index);
                              });
                            },
                          ))),
                )
            ],
            onReorder: (int oldIndex, int newIndex) {
              setState(() {
                if (oldIndex < newIndex) {
                  newIndex -= 1;
                }
                final Item item = Data.items.removeAt(oldIndex);
                Data.items.insert(newIndex, item);
              });
            }));
  }

  List<Widget> overlay() {
    List<Widget> widgets = [];
    widgets.add(pic);
    widgets.add(Positioned(
        left: xPos - 25,
        top: yPos - 25,
        child: Column(children: [
          Icon(
            Icons.shopping_cart,
            size: 50,
            color: Colors.redAccent,
          ),
        ])));
    for (Item i in Data.items) {
      widgets.add(Positioned(
          left: i.x - 5,
          top: i.y - 5,
          child: Icon(Icons.star, size: 10, color: Colors.black)));
    }

    return widgets;
  }

  Widget map(BoxConstraints size) {
    return Container(
        width: size.maxWidth * 0.8,
        child: InteractiveViewer(
            minScale: 0.1,
            maxScale: 10.0,
            constrained: false,
            transformationController: _transformationController,
            child: GestureDetector(
                onLongPressStart: (details) {
                  setState(() {
                    xPos = details.localPosition.dx;
                    yPos = details.localPosition.dy;
                  });
                  newItemDialog(context);
                },
                child: Stack(
                  children: overlay(),
                ))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => MyHomePage())),
        ),
      ),
      body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return Row(children: [itemList(constraints), map(constraints)]);
      }),
      floatingActionButton: Container(
        child: Row(
          children: <Widget>[
            Spacer(
              flex: 1,
            ),
            FloatingActionButton(
              onPressed: startUpload,
              tooltip: 'Save',
              child: Icon(Icons.save),
            ),
            Container(height: 20, width: 20),
            FloatingActionButton(
              backgroundColor: Colors.yellow,
              onPressed: getImage,
              tooltip: 'Pick Image',
              child: Icon(Icons.add_a_photo),
            ),
          ],
        ),
      ),
    );
  }
}
