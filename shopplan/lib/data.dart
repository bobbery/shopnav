class Item {
  Item(
      this.key, this.name, this.nameEN, this.costs, this.count, this.x, this.y);
  int key;
  String name;
  String nameEN;
  double costs;
  int count;
  double x;
  double y;

  Item.fromJson(Map<String, dynamic> json)
      : key = json['key'],
        name = json['name'],
        nameEN = json['nameEN'],
        costs = double.parse(json['costs'].toString()),
        count = json['count'],
        x = double.parse(json['x'].toString()),
        y = double.parse(json['y'].toString());

  Map<String, dynamic> toJson() => {
        'key': key,
        'name': name,
        'nameEN': nameEN,
        'costs': costs,
        'count': count,
        'x': x,
        'y': y,
      };
}

class Data {
  static List<Item> items = [
    Item(0, "EINGANG", "START", 0, 0, 15 * 3.0, 15 * 3.0),
    Item(1, "Tee", "Tea", 1.59, 0, 50 * 3.0, 68 * 3.0),
    Item(2, "Kornflakes", "Cornflakes", 0.97, 0, 50 * 3.0, 113 * 3.0),
    Item(3, "Brot", "Bread", 0.79, 0, 27 * 3.0, 125 * 3.0),
    Item(4, "Schokolade", "Chocolate", 0.49, 0, 50 * 3.0, 145 * 3.0),
    Item(5, "Wein", "Wine", 2.39, 0, 27 * 3.0, 170 * 3.0),
    Item(6, "Zahnpasta", "Toothpaste", 1.15, 0, 50 * 3.0, 255 * 3.0),
    Item(7, "Fleisch", "Meat", 2.59, 0, 130 * 3.0, 245 * 3.0),
    Item(8, "Pizza", "Pizza", 1.59, 0, 130 * 3.0, 180 * 3.0),
    Item(9, "KASSE", "END", 1.59, 0, 130 * 3.0, 20 * 3.0),
  ];
}

final String baseUrl = 'http://jr.marsch.tech/backend/';
//final String baseUrl = 'http://localhost/backend/';

class ShopItem {
  ShopItem(this.shopId, this.name, this.place, this.street);
  int shopId;
  String name;
  String place;
  String street;

  ShopItem.fromJson(Map<String, dynamic> json)
      : shopId = json['shopId'],
        name = json['name'],
        place = json['place'],
        street = json['street'];
}
