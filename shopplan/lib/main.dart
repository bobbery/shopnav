import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:splashscreen/splashscreen.dart';
import 'map.dart';
import 'data.dart';

void main() {
  runApp(MyApp());
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: MyHomePage(),
      title: Text(
        'ShopNav',
        textScaleFactor: 4,
      ),
      image: Image(image: AssetImage('img/logo.png')),
      loadingText: Text("Loading ..."),
      photoSize: 150.0,
      loaderColor: Colors.black,
      backgroundColor: Colors.lightGreen,
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ShopPlan',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: Splash(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  final String title = "ShopPlan";

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String name = "";
  String place = "";
  String street = "";
  int shopId = 0;

  List<ShopItem> shopItems = [];

  @override
  void initState() {
    super.initState();
    loadShops();
  }

  void loadShops() {
    http.get(Uri.parse(baseUrl + 'getRes.php?type=shops')).then((result) {
      print(result.statusCode);
      if (result.statusCode == 200) {
        print(jsonDecode(result.body));

        var tagObjsJson = jsonDecode(result.body) as List;

        setState(() {
          shopItems =
              tagObjsJson.map((tagJson) => ShopItem.fromJson(tagJson)).toList();
        });
      }
    });
  }

  createNewShop() {
    http.post(Uri.parse(baseUrl + 'newShop.php'), body: {
      "shopId": shopId.toString(),
      "name": name,
      "place": place,
      "street": street,
    }).then((result) {
      print(result.statusCode == 200 ? result.body : "ERROR");
      loadShops();
    }).catchError((error) {
      print(error);
    });
  }

  void newItemDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('New Shop'),
            content: Container(
              height: 180,
              width: 600,
              child: Column(
                children: [
                  TextField(
                    onChanged: (value) {
                      name = value;
                    },
                    decoration: InputDecoration(hintText: "Name"),
                  ),
                  TextField(
                    onChanged: (value) {
                      place = value;
                    },
                    decoration: InputDecoration(hintText: "Place"),
                  ),
                  TextField(
                    onChanged: (value) {
                      street = value;
                    },
                    decoration: InputDecoration(labelText: "Street"),
                  ),
                ],
              ),
            ),
            actions: [
              ElevatedButton(
                child: Text('ADD'),
                onPressed: () {
                  setState(() {
                    createNewShop();
                    Navigator.pop(context);
                  });
                },
              ),
              ElevatedButton(
                child: Text('CANCEL'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              )
            ],
          );
        });
  }

  List<Widget> shopList() {
    List<Widget> result = [];

    for (ShopItem item in shopItems) {
      result.add(ListTile(
        leading: Icon(Icons.storefront),
        title: Text(item.name),
        subtitle: Text(item.place + ', ' + item.street),
        onTap: () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => MapPage(item.shopId)));
        },
      ));
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: shopList(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          newItemDialog(context);
        },
        tooltip: 'New Shop',
        child: Icon(Icons.add),
      ),
    );
  }
}
