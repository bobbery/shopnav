<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
 
    $shopId = $_GET['shopId'];
    $type = $_GET['type'];
 
    $db = new SQLite3('test.db');

    if ($type=='shops')
    {
        $results = $db->query('SELECT * FROM shops');
        while ($row = $results->fetchArray()) {

            $jsonArray[] = $row;
        }

        echo json_encode($jsonArray);
        return;
    }
    
    
    $res = $db->query("SELECT * FROM " . $type . " WHERE shopId=" . $shopId);

    if ($row = $res->fetchArray())
    {
        if ($type == "images") 
        {
            header("Content-Type: image/png");

            echo base64_decode($row['data']);
        }
        else if ($type == "products")
        {
            echo $row['data'];
        }
    }
    else
    {
        echo "ERROR";
    }
 
?> 
